//comentarios

/* multiples líneas
linea 1
linea 2
linea 3
*/



/* 
==============================
variables
==============================
*/

/*
var nombre = "Daniel"
let edad = 41
const sexo = "Masculino"
console.log(nombre)
console.log(edad)
console.log(sexo)
*/


/* 
==============================
var
scope global
==============================
*/

/*
var ciudad = "Buenos Aires"
console.log(ciudad)
{
    console.log(ciudad)
}
console.log(ciudad)

//defino dentro de bloque puedo acceder fuera de él
{
    var activo = true
    console.log(activo)
}
console.log(activo)

//permite definir nueva variable con el mismo nombre
var altura = 1.70
var altura = 1.78
console.log(altura)
/*

/* 
==============================
let
scope de bloque
==============================
*/

/*

let ciudad = "Bogota"
console.log(ciudad)
{
    console.log(ciudad)
}
console.log(ciudad)

//defino dentro de bloque y solamente es accedible dentro de él
{
    let activo = true
    console.log(activo)
}
console.log(activo) //error: activo is not defined

//no permite definir nueva variable con el mismo nombre
let altura = 1.70
let altura = 1.78 //error: Identifier 'altura' has already been declared
console.log(altura)

//permite reasignar valores
let peso = 95
console.log(peso)
peso = 99
console.log(peso)


//alcanza a bloques hijos
let clima = null
{
    console.log(clima) //imprime null
    {
        let clima = "lluvioso"
        console.log(clima) //imprime lluvioso
        {
            let clima = "calido"
            {
                console.log(clima) //imprime calido
            }
        }
    }
}

*/

/* 
==============================
const
scope de bloque
==============================
*/

/*
const ciudad = "Ciudad de México"
console.log(ciudad)
{
    console.log(ciudad)
}
console.log(ciudad)


//defino dentro de bloque y solamente es accedible dentro de él
{
    const activo = true
    console.log(activo)
}
//console.log(activo) //error: activo is not defined



//no permite definir nueva variable con el mismo nombre
const altura = 1.70
//const altura = 1.78 //error: Identifier 'altura' has already been declared
console.log(altura)


//no permite reasignar valores
const peso = 95
console.log(peso)
//peso = 99 //error: Assignment to constant variable.
console.log(peso)


//alcanza a bloques hijos
//permite redefinir mismo nombre de variable en otro bloque
const clima = null
{
    console.log(clima) //imprime null
    {
        const clima = "lluvioso"
        console.log(clima) //imprime lluvioso
        {
            const clima = "calido"
            {
                console.log(clima) //imprime calido
            }
        }
    }
}
*/

/* 
==============================
hoisting
==============================
*/

/*
console.log(licenciaDeConducir) //undefined
var licenciaDeConducir = true

//no se puede acceder hasta que no haya un valor asignado
console.log(dni) //Error: Cannot access 'dni' before initialization
let dni = true

//no se puede acceder hasta que no haya un valor asignado
console.log(cedula) //Error: Cannot access 'cedula' before initialization
const cedula = true
*/

const persona = {
    "nombre": "Daniel",
    "apellido": "Segovia",
    fullName: function () {
        return `${this.nombre} ${this.apellido}`
    }
}; 
console.log(persona.fullName())

const persona2 = {
    "nombre": "Daniel",
    "apellido": "Segovia",
    fullName: () => {
        return `${this.nombre} ${this.apellido}`
    }
}; 
console.log(persona2.fullName())
